// 1.Тому що це не надійно, оскільки вхідні дані можуть надходити з різних джерел.
// Для цього у нас є події input та change для обробки будь-якого введення.


// const enter = document.getElementById("btn-enter");
// const btnS = document.getElementById("btn-s");
// const btnE = document.getElementById("btn-e");
// const btnO = document.getElementById("btn-o");
// const btnN = document.getElementById("btn-n");
// const btnL = document.getElementById("btn-l");
// const btnZ = document.getElementById("btn-z");

// let changeColor = function(button) {

//   buttons.forEach((elem) => {
//     elem.style.backgroundColor = "black";
//   });

//   button.style.backgroundColor = "blue";
// };

// document.addEventListener("keydown", (event) => {
//   if(event.code === "Enter") {
//     changeColor(enter);
//   };
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyS") {
//     changeColor(btnS);
//   };
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyE") {
//     changeColor(btnE);
//   };
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyO") {
//     changeColor(btnO);
//   }
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyN") {
//     changeColor(btnN);
//   }
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyL") {
//     changeColor(btnL);
//   }
// });

// document.addEventListener("keydown", (event) => {
//   if(event.code === "KeyZ") {
//     changeColor(btnZ);
//   }
// });

// const buttons = document.querySelectorAll(".btn");


const buttons = document.querySelectorAll(".btn");


document.addEventListener("keydown", (event) => {
  const key = event.key.toUpperCase();

  buttons.forEach((button) => {
    if (button.dataset.key === key || (button.dataset.key === "Enter" && event.code === "Enter")) {
      button.classList.add("blue");
    } else {
      button.classList.remove("blue");
    }
  });

});


